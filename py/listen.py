#!/usr/bin/env python
from __future__ import print_function

import logging
import sys
import time

from rtmidi.midiutil import open_midiinput

midiin, port_name = open_midiinput(1)

midiin.ignore_types(sysex=False)

print("opening "+port_name)

try:
    timer = time.time()
    while True:
        msg = midiin.get_message()

        if msg:
            message, deltatime = msg
            payload=""
            if message[0]==0xf0 and message[1]==0x7d:
                pos = 2
                while message[pos]!=0xf7:
                    payload += chr(message[pos]|message[pos+1]<<4)
                    pos+=2
                print(payload)
                           
        time.sleep(0.01)

finally:
    print("Exit.")
    midiin.close_port()
    del midiin
