// Copyright (C) 2019 FoAM Kernow
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "interpreter.h"

#ifndef PATTERN_MATRIX
#define PATTERN_MATRIX

#define SHAPE_CIRCLE 0
#define SHAPE_RECT 1
#define SHAPE_TRIANGLE 2
#define SHAPE_SQUARE 3

#define CIRCLE_1    0b0000
#define CIRCLE_2    0b1111
#define RECT_1      0b0101
#define RECT_2      0b1010
#define TRIANGLE_1  0b1100
#define TRIANGLE_2  0b0110
#define TRIANGLE_3  0b0011
#define TRIANGLE_4  0b1001
#define SQUARE_1    0b0001
#define SQUARE_2    0b0010
#define SQUARE_3    0b0100
#define SQUARE_4    0b1000
#define SQUARE_5    0b1110
#define SQUARE_6    0b1101
#define SQUARE_7    0b1011
#define SQUARE_8    0b0111

#define PATTERN_MATRIX_ID_START 0x0a
#define BLOCKS_W 4
#define BLOCKS_H 4

#define FILTER_HYPOTHESIS_AGE 10

typedef struct {
  unsigned char hypothesis;
  unsigned char current;
  unsigned int age;
} filter_token_t;

void filter_token_init(filter_token_t *f);

typedef struct {
  filter_token_t state[BLOCKS_W*BLOCKS_H];
} filter_matrix_t;

void filter_matrix_init(filter_matrix_t *f);

// todo: filter
unsigned char pm_read_block(unsigned int id);
unsigned char pm_block_shape(unsigned char token);
unsigned char pm_read_block_filtered(unsigned int id, filter_matrix_t *filter);
// ret needs to be BLOCKS_W*BLOCKS_H bytes
void pm_string_pattern(unsigned char *ret, filter_matrix_t *filter);
void pm_update_interpreter(sound_state *s, filter_matrix_t *filter);
void pm_debug_scan();

#endif
