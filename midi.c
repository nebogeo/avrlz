// Copyright (C) 2019 FoAM Kernow
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <avr/io.h>
#include <util/delay.h>
#include "midi.h"

#define USART_BAUDRATE 31250
#define ubrr (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)

// status bytes
#define MIDI_NOTE_ON 0x90
#define MIDI_NOTE_OFF 0x80
#define MIDI_CHANNEL_MODE 0xB0
#define MIDI_CONTROLLER 0xB0 // same as above

// channel mode messages
#define MIDI_ALL_SOUND_OFF     120
#define MIDI_RESET_CONTROLLERS 121
#define MIDI_LOCAL_CONTROL     122
#define MIDI_ALL_NOTES_OFF     123

// CCs
#define MIDI_CC_MODWHEEL_COURSE 1
#define MIDI_CC_MODWHEEL_FINE 33
#define MIDI_CC_VOLCAFM_TRANS 40
#define MIDI_CC_VOLCAFM_VEL   41
#define MIDI_CC_VOLCAFM_MOD_A 42
#define MIDI_CC_VOLCAFM_MOD_D 43
#define MIDI_CC_VOLCAFM_CAR_A 44
#define MIDI_CC_VOLCAFM_CAR_D 45
#define MIDI_CC_VOLCAFM_LFO_R 46
#define MIDI_CC_VOLCAFM_LFO_D 47
#define MIDI_CC_VOLCAFM_ALG   48
#define MIDI_CC_VOLCAFM_ARP_T 49
#define MIDI_CC_VOLCAFM_ARP_D 50

#define MIDI_SYSEX_START 0xf0
#define MIDI_SYSEX_END   0xf7
// "This ID is for educational or development use only, and should
// never appear in a commercial design."
#define MIDI_SYSEX_ID 0x7D

void midi_init() {
  DDRD = 0b00000010;    
  UCSR0B = (1 << TXEN0);   
  UCSR0C = (1 << UCSZ00)| (1 << UCSZ01); 
  UBRR0H = (unsigned char)(ubrr>>8);
  UBRR0L = (unsigned char)ubrr;
}

void midi_write(unsigned char data) {
  // wait for empty transmit buffer 
  while(!( UCSR0A & (1<<UDRE0)));
  // put data into buffer, sends the data
  UDR0 = data;
}

void midi_send_note_on(unsigned char channel, 
		       unsigned char note,
		       unsigned char velocity) {
  //printf("midi note on: %d %d %d\n", device, note, velocity);

  midi_write(MIDI_NOTE_ON|channel);
  midi_write(note);
  midi_write(velocity);
}

void midi_send_note_off(unsigned char channel, 
			unsigned char note) {
  //printf("midi note off: %d %d\n", device, note);
  midi_write(MIDI_NOTE_OFF|channel);
  midi_write(note);
  midi_write(0);
}

void midi_send_all_notes_off(unsigned char channel) {
  midi_write(MIDI_CHANNEL_MODE|channel);
  midi_write(MIDI_ALL_NOTES_OFF);
  midi_write(0);
}

void midi_send_cc(unsigned char channel, 
		  unsigned char num,
		  unsigned char value) {
  //printf("midi cc: %d %d %d\n", device, num, value);
  midi_write(MIDI_CONTROLLER|channel);
  midi_write(num);
  midi_write(value);
}

void midi_send_sysex(unsigned char *data, unsigned int size) {
  midi_write(MIDI_SYSEX_START);
  midi_write(MIDI_SYSEX_ID);
  for (unsigned int i=0; i<size; i++) {
    midi_write(data[i]);
  }
  midi_write(MIDI_SYSEX_END);
}

