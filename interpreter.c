// Copyright (C) 2019 FoAM Kernow
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "interpreter.h"
#include "midi.h"
#include <util/delay.h>

#define MAX_SIMULTANEOUS_NOTES 4

void variable_delay_ms(int n) {
  while(n--) {
    _delay_ms(1);
  }
}

void sound_state_init(sound_state *s) {
  s->note=36;
  s->timbral=64;
  s->scale=3;
  s->note_min=12*4; // measly two octaves...
  s->note_max=12*6;
  s->delay=500; // 120 bpm
  s->bar_restart=0;
  s->beat_count=0;
  s->timbral_mode=0;
  s->autoghost_mode=0;

  s->ghosting[0]=127;
  s->ghosting[1]=32;
  s->ghosting[2]=64;
  s->ghosting[3]=32;
  s->ghosting[4]=96;
  s->ghosting[5]=32;
  s->ghosting[6]=64;
  s->ghosting[7]=32;
}

unsigned int calc_midi_note(sound_state* s) {
  char *scale=scale_whole;
  switch (s->scale) {
  case 0: scale=scale_major; break;
  case 1: scale=scale_harmonic_minor; break;
  case 2: scale=scale_pentatonic_major; break;
  case 3: scale=scale_pentatonic_minor; break;
  case 4: scale=scale_aeolian; break;
  case 5: scale=scale_ethiopian; break;
  case 6: scale=scale_hindu; break;
  case 7: scale=scale_persian; break;
  default: scale=scale_major; break;
  }    
 
  unsigned int note = scale_to_note_compressed(s->note,scale,sizeof(scale));   

  note=note-s->note_min;
  note%=s->note_max-s->note_min;
  note+=s->note_min;

  return note;
}

int wrap(int v, int delta, int minval, int maxval) {
  const int mod = maxval + 1 - minval;
  if (delta >= 0) {return  (v + delta                - minval) % mod + minval;}
  else            {return ((v + delta) - delta * mod - minval) % mod + minval;}
}

unsigned char interpret(char token, sound_state *state) {
  switch(token) {
  case ' ': break;
  case '.': 
    stop_all(); 
    variable_delay_ms(state->delay);     
    state->beat_count++;
    break;
  case '+': state->timbral_mode=0; break;
  case '-': state->timbral_mode=1; break;
  case 'a': play_note(token, 1, state); state->notes_triggered++; break;
  case 'b': play_note(token, -1, state); state->notes_triggered++; break;
  case 'c': play_note(token, 1, state); state->notes_triggered++; break;
  case 'd': play_note(token, -1, state); state->notes_triggered++; break;
  case 'e': play_note(token, 1, state); state->notes_triggered++; break;
  case 'f': play_note(token, -1, state); state->notes_triggered++; break;
  case 'g': play_note(token, 1, state); state->notes_triggered++; break;
  case 'h': play_note(token, -1, state); state->notes_triggered++; break;
  }

  if (state->notes_triggered>MAX_SIMULTANEOUS_NOTES) {
    variable_delay_ms(state->delay);
    state->beat_count++;
    state->notes_triggered=0;
  }

  if (state->bar_restart>0 && state->beat_count>=state->bar_restart) {
    state->beat_count=0;
    return 1;
  }
  return 0;
}

void send_su10_note(unsigned int n, unsigned char param) {
  midi_send_note_off(0, last_su10_notes[last_su10_note_index]); 
  midi_send_note_on(0, n, param);
  last_su10_notes[last_su10_note_index] = n;
  last_su10_note_index++;
  last_su10_note_index=last_su10_note_index % su10_poly;
}

void play_note(unsigned char token, int param_change, sound_state* s) {
  unsigned int note = calc_midi_note(s);
  unsigned char velocity = 127;
  if (s->autoghost_mode) velocity=s->ghosting[s->beat_count%8];
  
  switch (token) { 
  case 'a': // volca
  case 'b': // volcafm
    midi_send_note_off(1, last_volcafm_notes[last_volcafm_note_index]); 
    midi_send_cc(1, MIDI_CC_VOLCAFM_VEL, velocity);
    midi_send_cc(1, MIDI_CC_VOLCAFM_MOD_A, s->timbral);
    midi_send_note_on(1, note, 127);      
    last_volcafm_notes[last_volcafm_note_index] = note;
    last_volcafm_note_index++;
    last_volcafm_note_index=last_volcafm_note_index % volcafm_poly;
    break;
  case 'c': // microbrute
  case 'd': // microbrute
    midi_send_all_notes_off(2);
    midi_send_cc(2, MIDI_CC_MODWHEEL_COURSE, s->timbral); 
    midi_send_note_on(2, note-12, velocity);
    break;
      
    // su10
    // 12 samples in the bank seem to be mapped from 36-47
    // ignore the scale and just use the raw note to figure it out
  case 'e':
    send_su10_note((s->timbral%12)+36,velocity);
    break;
  case 'f':
    send_su10_note(((s->timbral+1)%12)+36,velocity);
    break;
  case 'g':      
    send_su10_note(((s->timbral+2)%12)+36,velocity);
    break;
  case 'h': 
    send_su10_note(((s->timbral+3)%12)+36,velocity);
    break;      
  }

  // modify state... no note can repeat...
  if (s->timbral_mode) {
    s->timbral=wrap(s->timbral,param_change*20,0,127);
  } else {
    s->note=wrap(s->note,param_change,0,127);
  }

}

void stop_all() {
  //midi_send_note_off(2, last_volcafm_note); 
  //midi_send_note_off(1, last_microbrute_note); 
}

unsigned int scale_to_note_snapped(unsigned int note, char *intervals, unsigned int intervals_size) {
  // assume (seems to be right that 0 = C)
  // and scale intervals start at C
  
  unsigned int octave = note/12;
  unsigned int n = note%12;
  unsigned int out = 999;

  // search for n in intervals
  unsigned int valid=0;
  unsigned int last=0;

  for (unsigned int i=0; i<intervals_size; i++) {
    // if not found yet...
    if (out==999) {
      // snap out to the closest in this scale
      // exact note found
      if (n==valid) out=octave*12+valid;
      // we have gone past it
      if (n<valid) out=octave*12+last;
      last=valid;
      valid+=intervals[i]; // go to next valid note
    }
  }
  if (out==999) {
    out=octave*12+last;
  }
  
  return out;
}

unsigned int scale_to_note_compressed(unsigned int note, char *intervals, unsigned int intervals_size) {
  unsigned int out = 0;
  for (unsigned int i=0; i<note; i++) {
    out+=intervals[i%intervals_size]; 
  }
  return out;
}

