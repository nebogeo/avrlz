// Copyright (C) 2019 FoAM Kernow
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "lz.h"

void lz_init(lz_t *lz) {
  // clear the stack
  lz->stkptr=0;
  lz->recur_depth=16;
  for (unsigned int n=0; n<MAX_STACK_SIZE; n++) {
    lz->stack[n].pos=0;
    lz->stack[n].pat=0;
  }
  for (unsigned int n=0; n<MAX_PATTERN_SIZE*PATTERN_COUNT; n++) {
    lz->pattern[n]='.';
  }
}

void lz_reset(lz_t *lz) {
  lz->stkptr=0;
  for (unsigned int n=0; n<MAX_STACK_SIZE; n++) {
    lz->stack[n].pos=0;
    lz->stack[n].pat=0;
  }
}

unsigned char lz_peek(lz_t *lz) {
  lz_context_t ctx=lz->stack[lz->stkptr];
  return lz->pattern[ctx.pos+ctx.pat*PATTERN_COUNT];
}

void lz_push(lz_t *lz, unsigned int pos, unsigned int pat) {
  if (lz->stkptr<lz->recur_depth) {
    lz->stack[lz->stkptr].pos++;
    lz->stkptr++;
    lz->stack[lz->stkptr].pos=pos;
    lz->stack[lz->stkptr].pat=pat;
  } else {
    lz_inc_pos(lz);
  }
}

void lz_pop(lz_t *lz) {
  if (lz->stkptr>0) { 
    lz->stkptr--;
    // this means we might need to pop again!
    if (lz->stack[lz->stkptr].pos>MAX_PATTERN_SIZE-1) {
      lz_pop(lz);
    }
  }
  else lz->stack[lz->stkptr].pos=0; // loop the top pattern
}

void lz_inc_pos(lz_t *lz) {
  lz->stack[lz->stkptr].pos++;
  if (lz->stack[lz->stkptr].pos>MAX_PATTERN_SIZE-1) {
    lz_pop(lz); 
  }
}

unsigned char lz_step(lz_t *lz) {
  char musical = 0;
  unsigned char token=' ';
  while (!musical) { 
    token = lz_peek(lz);
    //printf("token: %c stkptr: %d pos: %d pat: %d\n",token,lz->stkptr,lz->stack[lz->stkptr].pos,lz->stack[lz->stkptr].pat);
    switch (token) {
    case 'W': lz_push(lz,0,0); break;
    case 'X': lz_push(lz,0,1); break;
    case 'Y': lz_push(lz,0,2); break;
    case 'Z': lz_push(lz,0,3); break;
    default: { 
      lz_inc_pos(lz);
      musical=1;
    }
    }
  }
  return token;
}

unsigned char lz_compare_pattern(unsigned char *a, unsigned char *b) {
  for (unsigned int i=0; i<MAX_PATTERN_SIZE*PATTERN_COUNT; i++) {
    if (a[i]!=b[i]) return 1;
  }
  return 0;
}

void lz_copy_pattern(unsigned char *s, unsigned char *d) {
  for (unsigned int i=0; i<MAX_PATTERN_SIZE*PATTERN_COUNT; i++) {
    d[i]=s[i];
  }
}
