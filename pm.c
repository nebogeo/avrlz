// Copyright (C) 2019 FoAM Kernow
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <stdio.h>
#include "pm.h"
#include "i2c-master.h"
#include "sysex.h"

#define PM_READ_ERROR 0xff
#define PM_SENSOR_ALIVE_COUNTER 4
#define PM_SENSOR_ID_CHECK 5

unsigned char pm_read_block(unsigned int id) {
  unsigned char addr=PATTERN_MATRIX_ID_START+id;
  unsigned char buf[6];
  // firmware on the sensors only allows us to read one byte at
  // a time!
  unsigned char err=0;
  for (unsigned char reg=0; reg<6; reg++) {
    if (!err) {
      err = i2c_read_reg((addr<<1),reg,&buf[reg],1);
    }
  }
  
  // check both read error and the internal ID returned
  if (err==0 && buf[PM_SENSOR_ID_CHECK]==addr) {
    return (buf[0]|buf[1]<<1|buf[2]<<2|buf[3]<<3);    
  }
  
  char str[68];
  sprintf(str,"i2c error -> %02x",addr);
  sysex(str);
  return PM_READ_ERROR;  
}

unsigned char pm_block_shape(unsigned char token) {
  switch (token) {
  case CIRCLE_1: return SHAPE_CIRCLE;
  case CIRCLE_2: return SHAPE_CIRCLE;
  case RECT_1: return SHAPE_RECT;
  case RECT_2: return SHAPE_RECT;
  case TRIANGLE_1: return SHAPE_TRIANGLE;
  case TRIANGLE_2: return SHAPE_TRIANGLE;
  case TRIANGLE_3: return SHAPE_TRIANGLE;
  case TRIANGLE_4: return SHAPE_TRIANGLE;
  case SQUARE_1: return SHAPE_SQUARE;
  case SQUARE_2: return SHAPE_SQUARE;
  case SQUARE_3: return SHAPE_SQUARE;
  case SQUARE_4: return SHAPE_SQUARE;
  case SQUARE_5: return SHAPE_SQUARE;
  case SQUARE_6: return SHAPE_SQUARE;
  case SQUARE_7: return SHAPE_SQUARE;
  default: return SHAPE_SQUARE;
  }
  return SHAPE_SQUARE;
}

void filter_token_init(filter_token_t *f) {
  f->hypothesis=CIRCLE_1;
  f->current=CIRCLE_1;
  f->age=0;
}

void filter_matrix_init(filter_matrix_t *f) {
  for (unsigned int n=0; n<BLOCKS_W*BLOCKS_H; n++) {
    filter_token_init(&f->state[n]);
  }
}

// attempt to filter out edge cases
unsigned char pm_read_block_filtered(unsigned int id, filter_matrix_t *filter) {
  unsigned char now=pm_read_block(id);
  // probably need to signal where this error has happened...
  if (now!=PM_READ_ERROR) {

    // no change/what we expect
    if (now==filter->state[id].hypothesis) {
      filter->state[id].age=0;
    } else { // there is a disagreement
    
      // a change has occured since last time
      if (now!=filter->state[id].current) {
	// reset the clock
	filter->state[id].age=0;
      } else {
	// disagreement with hypothesis, but current and now match

	unsigned int acceptance_age = FILTER_HYPOTHESIS_AGE;
	if (pm_block_shape(filter->state[id].hypothesis)!=
	    pm_block_shape(filter->state[id].current)) {
	  // block switches are less likely to happen so need more
	  // time to accept
	  acceptance_age*=2;
	}
				 
	// if it has been this same way for a while with no changes
	if (filter->state[id].age>acceptance_age) {
	  // accept the new hypothesis
	  filter->state[id].hypothesis=filter->state[id].current;
	} else {
	  // otherwise increment the age
	  filter->state[id].age++;
	}
      }
    }
    filter->state[id].current=now;
  }
  
  return filter->state[id].hypothesis;
}

// ret needs to be BLOCKS_W*BLOCKS_H bytes
void pm_string_pattern(unsigned char *ret, filter_matrix_t *filter) {
  // skip bottom state row
  for (unsigned int n=0; n<BLOCKS_W*(BLOCKS_H); n++) {
    unsigned char token = pm_read_block_filtered(n,filter);
    switch (token) {
    case CIRCLE_1: ret[n]=' '; break;
    case CIRCLE_2: ret[n]='.'; break;
    case RECT_1: ret[n]='+'; break;
    case RECT_2: ret[n]='-'; break;
    case TRIANGLE_1: ret[n]='W'; break;
    case TRIANGLE_2: ret[n]='X'; break;
    case TRIANGLE_3: ret[n]='Y'; break;
    case TRIANGLE_4: ret[n]='Z'; break; // dup
    case SQUARE_1: ret[n]='a'; break;
    case SQUARE_2: ret[n]='b'; break;
    case SQUARE_3: ret[n]='c'; break;
    case SQUARE_4: ret[n]='d'; break;
    case SQUARE_5: ret[n]='e'; break;
    case SQUARE_6: ret[n]='f'; break;
    case SQUARE_7: ret[n]='g'; break;
    case SQUARE_8: ret[n]='h'; break;
    default: ret[n]=' '; break;
    }
  }  
}

void pm_update_interpreter(sound_state *s, filter_matrix_t *filter) {
  // read bottom state row
  unsigned char vx_block = pm_read_block_filtered(12,filter);
  unsigned char sc_block = pm_read_block_filtered(13,filter);
  unsigned char br_block = pm_read_block_filtered(14,filter);
  unsigned char tm_block = pm_read_block_filtered(15,filter);

  if (pm_block_shape(sc_block)==SHAPE_SQUARE) {
    switch (sc_block) {
    case SQUARE_1: s->scale=0; break;
    case SQUARE_2: s->scale=1; break;
    case SQUARE_3: s->scale=2; break;
    case SQUARE_4: s->scale=3; break;
    case SQUARE_5: s->scale=4; break;
    case SQUARE_6: s->scale=5; break;
    case SQUARE_7: s->scale=6; break;
    case SQUARE_8: s->scale=7; break;
    }
  }

  if (pm_block_shape(br_block)==SHAPE_TRIANGLE) {
    switch (br_block) {
    case TRIANGLE_1: s->bar_restart=0; break;
    case TRIANGLE_2: s->bar_restart=16; break;
    case TRIANGLE_3: s->bar_restart=8; break;
    case TRIANGLE_4: s->bar_restart=4; break;
    }
  }

  if (pm_block_shape(tm_block)==SHAPE_TRIANGLE) {
    switch (sc_block) {
    case TRIANGLE_1: s->delay=200; break;
    case TRIANGLE_2: s->delay=300; break;
    case TRIANGLE_3: s->delay=500; break;
    case TRIANGLE_4: s->delay=800; break;
    }
  }
  
}

void pm_debug_scan() {
  char str[64];
  unsigned char err = 0;
  for (unsigned char addr=0x0a; addr<=0x19; addr++) {
    unsigned char buf[6];
    err = i2c_read_reg((addr<<1),0,&buf[0],1);
    if (err) {	  
      sprintf(str, "X %d",addr);
      sysex(str);
    } else {    
      for (unsigned char reg=0; reg<6; reg++) {
	if (!err) {
	  err = i2c_read_reg((addr<<1),reg,&buf[reg],1);
	}      
      }
      if (!err) {
	sprintf(str, "[%02x] %d %d %d %d %d %d",addr,buf[0],buf[1],buf[2],buf[3],buf[4],buf[5]);
	sysex(str);
      }
    }
  }
}
