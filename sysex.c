#include <string.h>
#include "midi.h"
#include "sysex.h"

// 64 byte max, for stack size
void sysex_send_8bit_data(unsigned char *data, unsigned int size) {
  if (size<64) {
    unsigned char buf[127];
    unsigned int buf_pos=0;
    for (unsigned int i=0; i<size; i++) {
      buf[buf_pos++]=data[i]&0x0f;
      buf[buf_pos++]=(data[i]>>4)&0x0f;
    }    
    midi_send_sysex(buf, buf_pos);
  }
}

void sysex(char *str) {
  sysex_send_8bit_data((unsigned char *)str,strlen(str));
}

