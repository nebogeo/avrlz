// Copyright (C) 2019 FoAM Kernow
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <stdio.h>
#include <string.h>
#include "interpreter.h"
#include "midi.h"
#include "sysex.h"
#include "lz.h"
#include "pm.h"
#include "i2c-master.h"
#include <util/delay.h>

int main() {
  midi_init();
  sysex("started midi");
  sysex("starting up i2c");
  i2c_init();
  sysex("i2c running");

  lz_t l;
  lz_init(&l);

  //strcpy((char *)l.pattern,"XY+Za.b-Wc.efg.h");
  //                        WWWWXXXXYYYYZZZZ
  //  strcpy((char *)l.pattern,"XXXYe..feZgh.XY.");
  // tworules: strcpy((char *)l.pattern,"X+bWa.-WaZ+b+.Y-");
  // strcpy((char *)l.pattern,"X+bYa.-Wa+Zab.b.");
  //strcpy((char *)l.pattern,"XaXYc..fdZc+.XYb");
  
 
  // long and out of tune?? strcpy((char *)l.pattern,"XXXXc+XYb..Za-b.");
  //strcpy((char *)l.pattern,"XXXXc+XYb..Za-b.");  
  //strcpy((char *)l.pattern,"XXXXe+XYa..Zf-b.");

  //strcpy((char *)l.pattern,"XZcYe.+fgZdh.XY-");
  //                        WWWWXXXXYYYYZZZZ
  //strcpy((char *)l.pattern,"XXYcZ+Y-aW-.ef.g");
  //strcpy((char *)l.pattern,"XXYZaf.gch.he.gf");

  // recorded:
  //strcpy((char *)l.pattern,"XWYZaf.Wch.-W+gf");

  
  //strcpy((char *)l.pattern,"XWeXc+XYbg.Za-b.");

  //strcpy((char *)l.pattern,"XXYZaa.cf.f.+W-Y");

  //strcpy((char *)l.pattern,"XXYZaa.cf.f.+W-Y");
  
  //                        WWWWXXXXYYYYZZZZ
  // fixed meander strcpy((char *)l.pattern,"a-XYee.cd.fZ+WdY");
  //strcpy((char *)l.pattern,"a-XYee.cd.fZ+WdY");

  
  sound_state s;
  sound_state_init(&s);
  s.delay=150;
  s.timbral_mode=0;
  s.autoghost_mode=1;
  s.bar_restart=0;
  
  filter_matrix_t f;
  filter_matrix_init(&f);

  // set up messages we need to send over sysex
  unsigned char sync_msg[4];
  sync_msg[0]='S';
  sync_msg[1]=0;

  unsigned char pattern_msg[17];
  pattern_msg[0]='M';
  unsigned char *old_pattern = &pattern_msg[1];
  lz_copy_pattern(l.pattern,old_pattern);
  
  while (1) {
    pm_string_pattern(l.pattern,&f);
    //pm_update_interpreter(&s,&f);

    // send a new matrix if a change has occurred
    if (lz_compare_pattern(l.pattern,old_pattern)) {
      sysex_send_8bit_data(pattern_msg,17);      
    }    

    // update with the current position
    sync_msg[2]=l.stack[l.stkptr].pat;
    sync_msg[3]=l.stack[l.stkptr].pos;
    sysex_send_8bit_data(sync_msg,4);
    
    if (interpret(lz_step(&l),&s)) {  
      lz_reset(&l); 
    }     

    //pm_debug_scan();    
    //_delay_ms(100);   

  }
  return 0;
}






