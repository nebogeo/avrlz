#include "midi.h"

#ifndef SYSEX
#define SYSEX

void sysex(char *str);
void sysex_send_8bit_data(unsigned char *data, unsigned int length);

#endif

