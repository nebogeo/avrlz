// Copyright (C) 2019 FoAM Kernow
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef MIDI
#define MIDI

#define MIDI_CC_MODWHEEL_COURSE 1
#define MIDI_CC_MODWHEEL_FINE 33
#define MIDI_CC_VOLCAFM_TRANS 40
#define MIDI_CC_VOLCAFM_VEL   41
#define MIDI_CC_VOLCAFM_MOD_A 42
#define MIDI_CC_VOLCAFM_MOD_D 43
#define MIDI_CC_VOLCAFM_CAR_A 44
#define MIDI_CC_VOLCAFM_CAR_D 45
#define MIDI_CC_VOLCAFM_LFO_R 46
#define MIDI_CC_VOLCAFM_LFO_D 47
#define MIDI_CC_VOLCAFM_ALG   48
#define MIDI_CC_VOLCAFM_ARP_T 49
#define MIDI_CC_VOLCAFM_ARP_D 50

#define MIDI_SYSEX_START 0xf0
#define MIDI_SYSEX_END   0xf7
// "This ID is for educational or development use only, and should
// never appear in a commercial design."
#define MIDI_SYSEX_ID 0x7D

void midi_init();

void midi_send_note_on(unsigned char channel, 
		       unsigned char note,
		       unsigned char velocity);

void midi_send_note_off(unsigned char channel, 
			unsigned char note);

void midi_send_all_notes_off(unsigned char channel);

void midi_send_cc(unsigned char channel, 
		  unsigned char num,
		  unsigned char value);

void midi_send_sysex(unsigned char *data, unsigned int size);

#endif
