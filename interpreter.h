// Copyright (C) 2019 FoAM Kernow
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef INTERPRETER
#define INTERPRETER

static char scale_harmonic_minor[] = {2, 1, 2, 2, 1, 3, 1};
static char scale_major[] = {2, 2, 1, 2, 2, 2, 1};
static char scale_whole[] = {2, 2, 2, 2, 2, 2};
static char scale_pentatonic_major[] = {2, 2, 3, 2, 3};
static char scale_pentatonic_minor[] = {3, 2, 2, 3, 2};
static char scale_pentatonic_blues[] = {3, 2, 1, 1, 3, 2};
static char scale_pentatonic_neutral[] =  {2, 3, 2, 3, 2};
static char scale_dorian[] =  {2, 1, 2, 2, 2, 1, 2};
static char scale_prygian[] =  {1, 2, 2, 2, 1, 2, 2};
static char scale_lydian[] = {2, 2, 2, 1, 2, 2, 1};
static char scale_mixolydian[] = {2, 2, 1, 2, 2, 1, 2};
static char scale_aeolian[] = {2, 1, 2, 2, 1, 2, 2};
static char scale_locrian[] = {1, 2, 2, 1, 2, 2, 2};
static char scale_arabian_a[] = {2, 1, 2, 1, 2, 1, 2, 1};
static char scale_arabian_b[] = {2, 2, 1, 1, 2, 2, 2};
static char scale_ethiopian[] = {2, 1, 2, 2, 1, 2, 2};
static char scale_hawaiian[] = {2, 1, 2, 2, 2, 2, 1};
static char scale_hindu[] = {2, 2, 1, 2, 1, 2, 2};
static char scale_jewish[] = {1, 1, 1, 2, 2, 2, 1, 2};
static char scale_persian[] = {1, 3, 1, 1, 2, 3, 1};

typedef struct {
  unsigned char note;
  unsigned char timbral;
  unsigned char scale;
  unsigned char note_min;
  unsigned char note_max;
  unsigned int delay;
  unsigned char notes_triggered;
  unsigned int bar_restart;
  unsigned int beat_count;
  unsigned char timbral_mode;
  unsigned char autoghost_mode;
  unsigned char ghosting[8]; 
} sound_state;

void sound_state_init(sound_state *s);

// 0 nothing
// 1 bar reset the lsystem
unsigned char interpret(char token, sound_state *state);

unsigned int scale_to_note(unsigned int note, char *intervals, unsigned int interval_size);
unsigned int scale_to_note_compressed(unsigned int note, char *intervals, unsigned int intervals_size);

#define volcafm_poly 4
static unsigned int last_volcafm_notes[volcafm_poly];
static unsigned int last_volcafm_note_index = 0;

static unsigned int last_microbrute_note = 0;

#define su10_poly 4
static unsigned int last_su10_notes[volcafm_poly];
static unsigned int last_su10_note_index = 0;


void play_note(unsigned char token, int param_change, sound_state* s);
void stop_all();

#endif
