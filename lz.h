// Copyright (C) 2019 FoAM Kernow
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef LZ
#define LZ

#define MAX_PATTERN_SIZE 4
#define MAX_STACK_SIZE 32
#define PATTERN_COUNT 4

typedef struct {
  unsigned int pos;
  unsigned int pat;
} lz_context_t;

typedef struct {  
  lz_context_t stack[MAX_STACK_SIZE];
  unsigned int stkptr;
  unsigned int recur_depth;
  unsigned char pattern[MAX_PATTERN_SIZE*PATTERN_COUNT];
} lz_t;

void lz_init(lz_t *lz);
void lz_reset(lz_t *lz);
unsigned char lz_peek(lz_t *lz);
void lz_push(lz_t *lz, unsigned int pos, unsigned int pat);
void lz_pop(lz_t *lz);
void lz_inc_pos(lz_t *lz);
unsigned char lz_step(lz_t *lz);
unsigned char lz_compare_pattern(unsigned char *a, unsigned char *b);
void lz_copy_pattern(unsigned char *s, unsigned char *d);

#endif
